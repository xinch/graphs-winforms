﻿namespace Graphs
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.graphDrawBtn = new System.Windows.Forms.Button();
            this.matrixPathDialog = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(525, 381);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // graphDrawBtn
            // 
            this.graphDrawBtn.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.graphDrawBtn.Location = new System.Drawing.Point(0, 421);
            this.graphDrawBtn.Name = "graphDrawBtn";
            this.graphDrawBtn.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.graphDrawBtn.Size = new System.Drawing.Size(525, 23);
            this.graphDrawBtn.TabIndex = 1;
            this.graphDrawBtn.Text = "Draw";
            this.graphDrawBtn.UseVisualStyleBackColor = true;
            this.graphDrawBtn.Click += new System.EventHandler(this.graphDrawBtn_Click);
            // 
            // matrixPathDialog
            // 
            this.matrixPathDialog.FileName = "openFileDialog1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 444);
            this.Controls.Add(this.graphDrawBtn);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "Graph";
            this.Resize += new System.EventHandler(this.Form1_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button graphDrawBtn;
        private System.Windows.Forms.OpenFileDialog matrixPathDialog;
    }
}

