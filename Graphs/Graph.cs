﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Graphs
{
    class Graph // описывает сам граф
    {
        public Points[] points { get; set; }
        public Edge[] edges { get; set; }

        public Graph(Points[] graph_points, Edge[] graph_edges)
        {
            points = graph_points;
            edges = graph_edges;
        }
    }

    class Points // описывает вершину графа
    {
        public int Name { get;set;}

        public Points(int name)
        {
            Name = name;
        }
    }

    class Edge // описывает ребро графа
    {
        public Points StartPoint { get; set; }
        public Points EndPoint { get; set; }
        public int EdgeWeight { get; set; }

        public Edge(Points start, Points end, int weight)
        {
            StartPoint = start;
            EndPoint = end;
            EdgeWeight = weight;
        }
    }
}
