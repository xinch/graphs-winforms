﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Graphs
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        public struct Coordinates
        {
            public int x;
            public int y;
        }

        static private string path;

        public void re_Draw()
        {
            pictureBox1.Width = this.Width-16;
            pictureBox1.Height = this.Height-100;
            if (path.Length > 0)
            pictureBox1.Image = GenGraph(pictureBox1.Width, pictureBox1.Height);
        }

        private static Bitmap GenGraph(int width, int height)
        {
            if (width <= 0 || height <= 0) return null;
            var graph = new Bitmap(width, height);
            var fmGr = Graphics.FromImage(graph);

            Pen arrow_pen = new Pen(Color.FromArgb(255, 0, 0, 0), 2);
            Pen vertex_pen = new Pen(Color.FromArgb(255, 0, 0, 255), 2);
            Font font = new System.Drawing.Font("Courier New", 10, FontStyle.Regular);

            Graph gr; // получаем класс граф, хранит в себе ребра с весами и вершины
            gr = get_Graph_Matrix();
         
            int vertex_number = get_Points_Count();
            int graph_radius;
            double vertex_grad = 360 / vertex_number;
            double vertex_rad = (vertex_grad * 3.14) / 180;
            Coordinates[] xy = new Coordinates[vertex_number];
            graph_radius = vertex_number * 17;

            for (int i = 0; i < vertex_number; i++) // рисуем по кругу вершины графа
            {
                xy[i].x = (int)(Math.Cos(vertex_rad * i) * graph_radius + width / 2);
                xy[i].y = (int)(Math.Sin(vertex_rad * i) * graph_radius + height / 2);
                fmGr.DrawString(Convert.ToString(i+1), font, Brushes.Black, xy[i].x-15, xy[i].y+7);
            }

            for (int i = 0; i < vertex_number; i++)
            {
                fmGr.DrawEllipse(vertex_pen, xy[i].x, xy[i].y, 26, 26);
            }

            for (int i = 0; i < gr.edges.Length; i++) // соединение вершин и расстановка весов ребер
            {
                fmGr.DrawLine(arrow_pen,  xy[gr.edges[i].EndPoint.Name-1].x+13, xy[gr.edges[i].EndPoint.Name-1].y+13, xy[gr.edges[i].StartPoint.Name-1].x+13, xy[gr.edges[i].StartPoint.Name-1].y+13);
                fmGr.DrawString(Convert.ToString(gr.edges[i].EdgeWeight), font, Brushes.Red, (xy[gr.edges[i].EndPoint.Name - 1].x + 13 + xy[gr.edges[i].StartPoint.Name - 1].x + 13) / 2, (xy[gr.edges[i].EndPoint.Name - 1].y + 13 + xy[gr.edges[i].StartPoint.Name - 1].y + 13) / 2-9);
            }   
      
                return graph;
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            re_Draw();

        }

        static private Graph get_Graph_Matrix()
        {
            string line;
            char delimiter = ' ';
            System.IO.StreamReader f = new System.IO.StreamReader(path);
            Points[] v = new Points[get_Points_Count()];
            Edge[] e = new Edge[get_Edges_Count()];

            for (int i = 0; i < get_Points_Count(); i++)
            {
                v[i] = new Points(i + 1);
            }
            int j = 0; int edge_count = 0;
                while ((line = f.ReadLine()) != null && edge_count!=get_Edges_Count())
                {
                    string[] words = line.Split(delimiter);
                    
                    for (int i = 0; i < words.Length; i++)
                    {
                        if (words[i]!=" ")
                            if (Convert.ToInt32(words[i]) > 0)
                            {
                                e[edge_count] = new Edge(v[j], v[i], Convert.ToInt32(words[i]));
                                edge_count++;
                            }
                    }
                    j++;
                }
                f.Close();
                return new Graph(v, e);
        } // возвращает экземпляр Grap, хранящий вершины/ребра

        static private int get_Points_Count() // определяем количество вершин
        {
            string line;
            System.IO.StreamReader f = new System.IO.StreamReader(path);
            line = f.ReadLine();
            string[] words = line.Split(' ');
            f.Close();
            return words.Length;
        }

        static private int get_Edges_Count() // определяем количество ребер
        {
            int j = 0;
            string[] lines = System.IO.File.ReadAllLines(path);
            foreach (string s in lines)
            {
                foreach (char ch in s)
                {
                    if (ch!=' ') 
                        if (Convert.ToInt32(Convert.ToString (ch))> 0) j++;
                }
            } 
            return j;
        }

        private void graphDrawBtn_Click(object sender, EventArgs e)
        {
            OpenFileDialog dial = new OpenFileDialog();
            //dial.Filter = "txt (*.txt)";
            dial.Title = "Select text file";
            if (dial.ShowDialog() == DialogResult.OK)
            {
                path = dial.FileName;
                pictureBox1.Image = GenGraph(pictureBox1.Width, pictureBox1.Height);
            }

        }

    }
}
